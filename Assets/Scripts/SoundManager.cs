﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
  enum SoundType { BGM, SFX }
  public static SoundManager instance { get; private set; }

  [SerializeField] AudioClip[] musicClips;
  [SerializeField] AudioClip[] soundClips;
  [SerializeField] AudioSource _bgmSource;
  [SerializeField] AudioSource _sfxSource;

  public AudioSource bgmSource { get { return _bgmSource; } }
  public AudioSource sfxSource { get { return _sfxSource; } }

  void Awake()
  {
    if (instance == null) {
      instance = this;
    } else if (instance != this) {
      Destroy(gameObject);
    }
  }

  public static void PlayMusic(string name)
  {
    instance.bgmSource.Stop();
    instance.bgmSource.clip = GetClipByName(name, SoundType.BGM);
    instance.bgmSource.Play();
  }

  public static void PlaySound(string name)
  {
    instance.sfxSource.PlayOneShot(GetClipByName(name, SoundType.SFX));
  }

  public static bool IsMusicPlaying {
    get {
      return instance.bgmSource.isPlaying;
    }
  }

  public static void StopMusic()
  {
    instance.bgmSource.Stop();
  }

  static AudioClip GetClipByName(string name, SoundType type)
  {

    if (type == SoundType.BGM) {
      foreach (AudioClip clip in instance.musicClips) {
        if (name == clip.name) {
          return clip;
        }
      }
    } else {
      foreach (AudioClip clip in instance.soundClips) {
        if (name == clip.name) {
          return clip;
        }
      }
    }

    Debug.Log("Clip: " + name + " was not found.");
    return null;
  }

}
