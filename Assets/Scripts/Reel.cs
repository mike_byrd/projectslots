﻿using UnityEngine;

public class Reel : MonoBehaviour 
{
  public delegate void ReelEvent (Reel reel, ReelEventType eventType);
  public static event ReelEvent OnReelEvent;

  public Transform slotTransformer;
  public float spinSpeed = 5.0f;

  public bool isSpinning { get; private set; }

  public int[] iconData { get; set; }

  Icon[] icons;
  bool stopRequested = false;

  public Animator animator { get; private set; }

  const int MAX_POS_Y = -170;

  void Awake()
  {
    animator = GetComponent<Animator>();
    animator.enabled = false;
  }

	// Use this for initialization
	void Start () {
    // iconData should be set from ReelController in Awake.
    icons = gameObject.GetComponentsInChildren<Icon>();
    UpdateIconSprites();
  }

  // Update is called once per frame
  void Update () 
  {
    if (isSpinning) {
      slotTransformer.Translate(Vector2.down * ReelController.instance.reelSpinSpeed);

      if (slotTransformer.localPosition.y <= MAX_POS_Y) {
        // Reset the reel position
        var newPos = slotTransformer.localPosition;
        newPos.y = 0;
        slotTransformer.localPosition = newPos;

        // Update icon data so the new icon is in the list
        AddNewIcon();

        // Update visuals.
        UpdateIconSprites();
       
        if (stopRequested) {
          isSpinning = false;
          stopRequested = false;

          if (OnReelEvent != null) {
            OnReelEvent(this, ReelEventType.Stopped);

            if (animator) {
              animator.enabled = true;
              animator.SetTrigger("Bounce");
            }
          }
        }
      }
    }
  }

  void AddNewIcon()
  {
    // Grab a new icon and offset the iconData by 1.
    int newIconData = ReelController.GetNewIconData();
    int[] newIcons = new int[iconData.Length];

    // Assign the new icon to the last element in the array.
    newIcons[0] = newIconData;

    for (int i = 1; i < iconData.Length; i++) {
      newIcons[i] = iconData[i - 1];
    }

    // replace iconData
    iconData = newIcons;
  }

  void UpdateIconSprites()
  {
    for (int i = 0; i < icons.Length; i++) {
      icons[i].SetSprite(ReelController.GetIconSpriteAtIndex(iconData[i]));
    }
  }

  public void StartSpin()
  {
    if (isSpinning) return;

    isSpinning = true;
    OnReelEvent(this, ReelEventType.Started);
  }

  public void StopSpin()
  {
    if (!isSpinning) return;

    stopRequested = true;
    OnReelEvent(this, ReelEventType.StopRequested);
  }

  public void OnBounceEnd()
  {
    if (animator == null) return;

    animator.enabled = false;
    ReelController.instance.UpdateStoppedReelsCount();
  }

  public void OnBounceStart()
  {
    MainGame.instance.SetPlayButtonEnabled(false);
  }
}
