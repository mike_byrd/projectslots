﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ReelController : MonoBehaviour 
{
  public static ReelController instance { get; private set; }

  [SerializeField]
  Sprite[] iconSprites;

  [SerializeField]
  Reel[] reels;

  public int reelSpinSpeed { get { return _reelSpinSpeed; } }

  [SerializeField]
  int _reelSpinSpeed = 20;

  int stoppedReelsCount = 0;

  public bool areReelsSpinning { get; private set; }

  void Awake()
  {
    if (!instance) {
      instance = this;
    } else if (instance != this) {
      Destroy(gameObject);
      return;
    }

    InitializeReels();
  }

  void OnEnable() { Reel.OnReelEvent += OnReelEvent; }

	// Use this for initialization
	void Start () {
    
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  void InitializeReels()
  {
    areReelsSpinning = false;

    for (int i = 0; i < reels.Length; i++) {
      reels[i].iconData = GenerateIconData();
    }
  }

  public static Sprite GetIconSpriteAtIndex(int index)
  {
    if (index < 0 || index >= instance.iconSprites.Length) {
      Debug.LogWarning("Invalid sprite index: " + index);
      return null; 
    }

    return instance.iconSprites[index];
  }

  public int[] GenerateIconData()
  {
    int[] iconData = new int[4];
    for(int i = 0; i < iconData.Length; i++) {
      iconData[i] = GetNewIconData();
    }

    return iconData;
  }

  public static int GetNewIconData()
  {
    return Random.Range(0, instance.iconSprites.Length);
  }

  public void ToggleReelSpin()
  {
    areReelsSpinning = !areReelsSpinning;

    Reel reel;
    for (int i = 0; i < reels.Length; i++) {
      reel = reels[i];
      if (reel.isSpinning) {
        StartCoroutine(StartSpinningStopRoutine(i));
      } else {
        StartCoroutine(StartSpinningRoutine(i));
      }
    }

    if (areReelsSpinning) {
      MainGame.instance.SetButtonText("Stop");
      MainGame.instance.SetPlayButtonEnabled(false);
      if (!SoundManager.IsMusicPlaying) {
        SoundManager.PlayMusic(GameConstants.BGM_START_SPIN);
      }
    } else {
      MainGame.instance.SetButtonText("Play");
      MainGame.instance.SetPlayButtonEnabled(false);
    }
  }

  IEnumerator StartSpinningRoutine(int reelIndex)
  {
    yield return new WaitForSeconds(.2f * reelIndex);

    Reel reel = reels[reelIndex];
    reel.StartSpin();

    //Enable play button if this is the last reel.
    if (reelIndex == reels.Length - 1) {
      MainGame.instance.SetPlayButtonEnabled(true);
    }
  }

  IEnumerator StartSpinningStopRoutine(int reelIndex)
  {
    yield return new WaitForSeconds(.4f * reelIndex);

    Reel reel = reels[reelIndex];
    reel.StopSpin();
  }

  public void UpdateReelSpinSpeed(Slider slider)
  {
    instance._reelSpinSpeed = (int)slider.value;
    MainGame.instance.UpdateSpeedText();
  }

  public void UpdateStoppedReelsCount()
  {
    stoppedReelsCount++;
    if (stoppedReelsCount == reels.Length) {
      MainGame.instance.SetPlayButtonEnabled(true);
      stoppedReelsCount = 0;
    }
  }

  void OnReelEvent(Reel reel, ReelEventType eventType)
  {
    switch (eventType) {
      case ReelEventType.Started:
        break;
      case ReelEventType.StopRequested:
        SoundManager.StopMusic();
        SoundManager.PlaySound(GameConstants.SFX_STOP_SPIN);
        break;
      case ReelEventType.Stopped:
        break;
    }
  }
}
