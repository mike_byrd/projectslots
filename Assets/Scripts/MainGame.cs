﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainGame : MonoBehaviour 
{
  public static MainGame instance { get; private set; }
  public bool isPlayButtonEnabled { get; private set; }

  [SerializeField]
  ReelController reelController;

  [SerializeField]
  Button playButton;
  Text playButtonText;

  [SerializeField]
  Slider speedSlider;

  [SerializeField]
  Text speedText;
   
  void Awake()
  {
    if (instance == null) {
      instance = this;
    } else if (instance != this) {
      Destroy(gameObject);
      return;
    }

    playButtonText = playButton.GetComponentInChildren<Text>();
    isPlayButtonEnabled = true;

    Random.InitState((int)System.DateTime.Now.Ticks);
  }

  // Use this for initialization
  void Start () 
  {
    speedSlider.value = ReelController.instance.reelSpinSpeed;
    speedText.text = "Speed: " + speedSlider.value;
	}
	
	// Update is called once per frame
	void Update () 
  {
    
  }

  public void SetButtonText(string text)
  {
    playButtonText.text = text;
  }

  public void UpdateSpeedText()
  {
    speedText.text = "Speed: " + speedSlider.value;
  }

  public void OnPlayButtonClicked()
  {
    reelController.ToggleReelSpin();
  }

  public void SetPlayButtonEnabled(bool enabled)
  {
    if (enabled) {
      playButton.enabled = true;
      playButton.image.color = new Color(1, 1, 1, 1);
    } else {
      playButton.enabled = false;
      playButton.image.color = new Color(1, 1, 1, .7f);
    }
  }
}
