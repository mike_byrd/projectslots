﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Icon : MonoBehaviour 
{
  Image image;

  void Awake()
  {
    image = GetComponent<Image>();
  }

  // Use this for initialization
  void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  public void SetSprite(Sprite sprite)
  {
    image.sprite = sprite;
  }
}
