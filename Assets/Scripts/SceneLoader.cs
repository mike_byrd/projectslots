﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour 
{
  public static SceneLoader instance { get; private set; }

  string currentSceneName;
  string nextSceneName;
  AsyncOperation resourceUnloadTask;
  AsyncOperation sceneLoadTask;
  enum SceneState { Reset, Preload, Load, Unload, Postload, Ready, Run, Count };
  SceneState sceneState;
  delegate void UpdateDelegate();
  UpdateDelegate[] updateDelegates;

  /// <summary>
  /// Switches to a new scene.
  /// </summary>
  /// <param name="nextSceneName">The scene to switch to.</param>
  public static void SwitchScene(string nextSceneName, bool showLoadingProgress = false) 
  {
    if (instance != null) {
      if (instance.currentSceneName != nextSceneName) {
        instance.nextSceneName = nextSceneName;
      }
    }
    else {
      Debug.LogError("MainController.SwitchScene: Main Controller is null.");
    }
  }

  protected void Awake() 
  {
    //Setup the singleton instance
    if (!instance) {
      instance = this;
    }
    else if (instance != this) {
      Destroy(transform.gameObject);
      return;
    }

    //Setup the array of updateDelegates
    updateDelegates = new UpdateDelegate[(int)SceneState.Count];

    //Set each updateDelegate
    updateDelegates[(int)SceneState.Reset] = UpdateSceneReset;
    updateDelegates[(int)SceneState.Preload] = UpdateScenePreload;
    updateDelegates[(int)SceneState.Load] = UpdateSceneLoad;
    updateDelegates[(int)SceneState.Unload] = UpdateSceneUnload;
    updateDelegates[(int)SceneState.Postload] = UpdateScenePostload;
    updateDelegates[(int)SceneState.Ready] = UpdateSceneReady;
    updateDelegates[(int)SceneState.Run] = UpdateSceneRun;

    //set the next scene to the current scene.
    currentSceneName = SceneManager.GetActiveScene().name;
    nextSceneName = currentSceneName;
    sceneState = SceneState.Ready;
  }

  protected void Update() 
  {
    if (updateDelegates[(int)sceneState] != null) {
      updateDelegates[(int)sceneState]();
    }
  }

  protected void OnDestroy() 
  {
    //Clean up all the updateDelegates
    if (updateDelegates != null) {
      for (int i = 0; i < (int)SceneState.Count; i++) {
        updateDelegates[i] = null;
      }
      updateDelegates = null;
    }
  }

  // attach the new scene controller to start cascade of loading
  void UpdateSceneReset() 
  {
    // run a gc pass
    System.GC.Collect();
    sceneState = SceneState.Preload;
  }

  // handle anything that needs to happen before loading
  void UpdateScenePreload() 
  {
    sceneLoadTask = SceneManager.LoadSceneAsync(nextSceneName);
    sceneState = SceneState.Load;
  }

  // show the loading screen until it's loaded
  void UpdateSceneLoad() 
  {
    // done loading?
    if (sceneLoadTask.isDone == true) {
      sceneState = SceneState.Unload;
    }
    else {
      // update scene loading progress      
    }
  }

  // clean up unused resources by unloading them
  void UpdateSceneUnload() 
  {
    // cleaning up resources yet?
    if (resourceUnloadTask == null) {
      resourceUnloadTask = Resources.UnloadUnusedAssets();
    }
    else {
      // done cleaning up?
      if (resourceUnloadTask.isDone == true) {
        resourceUnloadTask = null;
        sceneState = SceneState.Postload;
      }
    }
  }

  // handle anything that needs to happen immediately after loading
  void UpdateScenePostload() 
  {
    currentSceneName = nextSceneName;
    sceneState = SceneState.Ready;
  }

  // handle anything that needs to happen immediately before running
  void UpdateSceneReady() 
  {
    /* run a gc pass
       if you have assets loaded in the scene that are
       currently unused but may be used later
       DON'T do this here
    */
    System.GC.Collect();
    sceneState = SceneState.Run;
  }

  // wait for scene change
  void UpdateSceneRun() 
  {
    if (currentSceneName != nextSceneName) {
      sceneState = SceneState.Reset;
    }
  }
}
