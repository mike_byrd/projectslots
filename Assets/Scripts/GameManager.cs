﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
  public static GameManager instance { get; private set; }

  void Awake()
  {
    if (!instance) {
      instance = this;
    } else if(instance != this) {
      Destroy(gameObject);
      return;
    }
  }

  void GenerateSlotData()
  {
    
  }

  // Use this for initialization
  void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
